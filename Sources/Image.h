#ifndef HC_Image_H
#define HC_Image_H

#include <vector>
#include <iostream>

#include <cstdint>
#include <cstddef>

namespace hc
{

    struct Image
    {
        int w;
        int h;
        std::vector<bool> data;

        int CountNumPixels() const;

        void Init(int iWidth, int iHeight);
        void Load(std::istream& ioStream);
        void Print(std::ostream& ioStream);
    };



    ///////////////////////////////////////////////////////////////////////////
    /// Implementation
    int Image::CountNumPixels() const
    {
        int numPixels = 0;
        for (auto p : data)
        {
            if (p)
                ++numPixels;
        }
        return numPixels;
    }

    void Image::Init(int iWidth, int iHeight)
    {
        h = iHeight;
        w = iWidth;
        data.resize(0);
        data.resize(h * w, false);
    }


    void Image::Load(std::istream& ioStream)
    {
        ioStream >> h;
        ioStream >> w;

        data.resize(0);
        data.reserve(h * w);
        std::vector<char> buffer(w + 256);
        ioStream.getline(buffer.data(), buffer.size());
        for (std::size_t i = 0; i < std::size_t(h); ++i)
        {
            ioStream.getline(buffer.data(), buffer.size());

            for (std::size_t c = 0; c < std::size_t(w); ++c)
            {
                data.push_back((buffer[c] == '#'));
            }

        }
    }

    void Image::Print(std::ostream& ioStream)
    {
       std::size_t c = 0;

        for (std::size_t i = 0; i < w; ++i)
        {
            for (std::size_t j = 0; j < h; ++j)
            {
                ++c;
                ioStream << data[c] ? "#" : ".";
            }
            ioStream << std::endl;
        }

    }


} // namespace hc

#endif
