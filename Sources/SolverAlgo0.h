#ifndef HC_SolverAlgo0_H
#define HC_SolverAlgo0_H
#include <utility>
#include <memory>

#include "Solver.h"

namespace hc
{

    struct SolverAlgo0 : public Solver
    {
        virtual void Process(World&) override;
        int ComputeCost(World & , World::Customer c, World::Warehouse w);
        void ComputeList(World::Customer & c, World::Warehouse & w, std::vector<int> & product);

    };



    ///////////////////////////////////////////////////////////////////////////
    /// Implementation
    int SolverAlgo0::ComputeCost(World & world, World::Customer c, World::Warehouse w)
    {
    	int n = 0;
    	for(size_t i = 0; i < c.orderedProducts.size(); i++)
    	{	
    		n += std::min(c.orderedProducts[i], w.availableProducts[i]);
    	}
    	return n + world.ComputeDistance(c.position, w.position);
    }


    // void SolverAlgo0::Decrement(World::Customer & c, World::Warehouse & w)
    // {
    // 	for(size_t i = 0; i < c.orderedProducts.size(); i++)
    // 	{	
    // 		n = std::min(c.orderedProducts[i], w.availableProducts[i]);
    // 		c.orderedProducts[i] -= n;
    // 		w.availableProducts[i] -= n;
    // 
    // 	}
    // }

    void SolverAlgo0::ComputeList(World::Customer & c, World::Warehouse & w, std::vector<int> & product)
    {
    	for(size_t i = 0; i < c.orderedProducts.size(); i++)
    	{	
    		int n = std::min(c.orderedProducts[i], w.availableProducts[i]);
    		product[i] = n;
    	}
    }

    void SolverAlgo0::Process(World& world)
    {
        int maxWeight = world.maxWeight;
    	for(unsigned int k = 0; k < world.customers.size(); k++)
    	{	
    		World::Customer c = world.customers[k];
    		while(c.numProducts > 0)
    		{
    			std::vector<World::Warehouse> warehouseList;
	    		{
		    		//selection du meilleur dépot
	    		    int idBestWarehouse = -1;
                    {
		    		    int maxc = -1;
		    		    //World::Warehouse bestWarehouse;

		    		    for(size_t i = 0; i < world.warehouses.size(); i++)
		    		    {
		    		    	int cout = ComputeCost(world, c, world.warehouses[i]);
		    		    	if(cout > maxc)
		    		    	{
		    		    		maxc = cout;
		    		    		idBestWarehouse = i;
		    		    		//bestWarehouse = world.warehouses[i];
		    		    	}
		    		    }
                    }
		    		//selection du meilleur drone
				    int idBestDrone = -1;
                    {
					    int minc = 1 << 30;
		    		    //World::Drone bestDrone;
		    		    for(size_t i = 0; i < world.drones.size(); i++)
		    		    {

		    		    	int cout = world.ComputeDistance(world.drones[i].position, world.warehouses[idBestWarehouse].position) + world.drones[i].lifeTime;
		    		    	if(cout < minc)
		    		    	{
		    		    		minc = cout;
		    		    		//bestDrone =  world.drones[i];
		    		    		idBestDrone = i;
		    		    	}
                        }
		    		}
		    		bool flag(false);
		    		std::vector<int> productList(world.productWeights.size(), 0);
		    		ComputeList(c, world.warehouses[idBestWarehouse], productList);
                    auto& bestDrone = world.drones[idBestDrone];

		    		for(size_t j = 0; j < productList.size(); j++)
		    		{
                        if(productList[j] != 0 && world.productWeights[j] < (maxWeight - bestDrone.weight))
		    			{
		    				int nmax = (maxWeight - bestDrone.weight) / world.productWeights[j];
		    				productList[j] = std::min(productList[j], nmax);
		    				if ( bestDrone.ComputeLoadTime(world.warehouses[idBestWarehouse]) + world.drones[idBestDrone].lifeTime < world.maxTurn)
		    				{
		    					flag = true;
		    					// productList[j] = 0;
		    					auto instruction = bestDrone.Load(world.warehouses[idBestWarehouse], j , productList[j]);
                                world.InsertInstruction(instruction);
		    				}
                            else
                                productList[j] = 0;
		    			}
		    		}
		    		for(size_t j = 0; j < productList.size(); j++)
		    		{
		    			if(productList[j] != 0 )
		    			{
		    				if(bestDrone.ComputeDeliverTime(world.customers[k]) + world.drones[idBestDrone].lifeTime < world.maxTurn )
		    				{
		    					auto instruction = world.drones[idBestDrone].Deliver(world.customers[k], j, productList[j]);
                                world.InsertInstruction(instruction);
		    				}
		    			}
		    		}
		    		if(!flag)
		    			break;

				}
    		
       		}
    	}
    }


} // namespace hc

#endif
