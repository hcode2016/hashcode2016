#define NDefine

#include "Solver.h"
#include "SolverAlgo0.h"

#include "World.h"

#include <iostream>
#include <fstream>

#include <memory>


int main(int nargs, const char** args)
{

    if (nargs != 3)
    {
        std::cout << "usecase :" << std::endl;
        std::cout << "hcode input_file_name output_filename" << std::endl;
        return 0;
    }

    std::cout << "input file: " << args[1] << std::endl;
    std::cout << "output file: " << args[2] << std::endl;

    std::ifstream inputFile(args[1]);
    std::ofstream outputFile(args[2]);


    hc::World worldSource;
    worldSource.Initialize(inputFile);

    std::unique_ptr<hc::Solver> solver(new hc::SolverAlgo0());
    solver->Process(worldSource);

    // worldSource.Write(outputFile);
    worldSource.Write(outputFile);
    // std::cout << worldSource.instructions.size();
    return 0;
};
