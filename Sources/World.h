#ifndef HC_World_H
#define HC_World_H

#include "Instruction.h"

#include <vector>
#include <iostream>

#include <utility>

namespace hc
{

    struct World
    {
        struct Customer
        {
            int id;

            int position;
            int numProducts;
            std::vector<int> orderedProducts;
        };

        struct Warehouse
        {
            int id;

            int position;
            std::vector<int> availableProducts;
        };

        struct Drone
        {
            int id;

            int position;
            const World* world;
            int weight;
            std::vector<int> loadedProducts;
            int lifeTime;

            // returns -1 if invalid cost
            int ComputeLoadTime(const Warehouse& iWarehouse) const;
            int ComputeDeliverTime(const Customer& iCustomer) const;

            Instruction Load(Warehouse& ioWarehouse, int iProductId, int iQuatity);
            Instruction Deliver(Customer& iCustomer, int iProductId, int iQuatity);
        };

        int width;
        int height;
        int maxWeight;
        int maxTurn;

        std::vector<Drone> drones;
        std::vector<Warehouse> warehouses;
        std::vector<Customer> customers;
        std::vector<int> productWeights;

        std::vector<Instruction> instructions;

        void Initialize(std::istream& ioInput);
        void Write(std::ostream& ioOutput) const;
        void InsertInstruction(const Instruction& iInstruction);

        int ComputeDistance(int iPosition0, int iPosition1) const;
        static int ComputeDistance(int iPosition0, int iPosition1, int iWidth);
        int ComputePosition(int iR, int iC) const;
    };


    ///////////////////////////////////////////////////////////////////////////
    /// Implementation
    int World::Drone::ComputeLoadTime(const Warehouse& iWarehouse) const
    {
        return 1 + world->ComputeDistance(position, iWarehouse.position);
    }

    int World::Drone::ComputeDeliverTime(const Customer& iCustomer) const
    {
        return 1 + world->ComputeDistance(position, iCustomer.position);
    }


    Instruction World::Drone::Load(Warehouse& ioWarehouse, int iProductId, int iQuatity)
    {
        Instruction instruction;
        instruction.type = Instruction::Type::eLoad;
        instruction.drone = id;
        instruction.product = iProductId;
        instruction.quantity = iQuatity;
        instruction.warehouse = ioWarehouse.id;

        weight += iQuatity * world->productWeights[iProductId];
        lifeTime += ComputeLoadTime(ioWarehouse);
        position = ioWarehouse.position;
        ioWarehouse.availableProducts[iProductId] -= iQuatity;
        loadedProducts[iProductId] += iQuatity;

        return instruction;
    }

    Instruction World::Drone::Deliver(Customer& ioCustomer, int iProductId, int iQuatity)
    {
        Instruction instruction;
        instruction.type = Instruction::Type::eDeliver;
        instruction.drone = id;
        instruction.product = iProductId;
        instruction.quantity = iQuatity;
        instruction.customer = ioCustomer.id;

        weight -= iQuatity * world->productWeights[iProductId];
        lifeTime += ComputeDeliverTime(ioCustomer);
        position = ioCustomer.position;
        ioCustomer.orderedProducts[iProductId] -= iQuatity;
        loadedProducts[iProductId] -= iQuatity;
        ioCustomer.numProducts -= iQuatity;

        return instruction;
    }

    void World::Initialize(std::istream& ioInput)
    {
        while (ioInput)
        {
            int numDrones = 0;
            int numProducts = 0;
            int numWarehouses = 0;
            int numCustomers = 0;

            ioInput >> height;
            ioInput >> width;
            ioInput >> numDrones;
            ioInput >> maxTurn;
            ioInput >> maxWeight;

            ioInput >> numProducts;
            productWeights.resize(numProducts);
            for (int i = 0; i < numProducts; ++i)
                ioInput >> productWeights[i];

            ioInput >> numWarehouses;
            warehouses.resize(numWarehouses);
            for (int i = 0; i < numWarehouses; ++i)
            {
                warehouses[i].id = i;

                int r0 = 0;
                int c0 = 0;

                ioInput >> r0;
                ioInput >> c0;
                warehouses[i].position = ComputePosition(r0, c0);
                warehouses[i].availableProducts.resize(numProducts);
                for (int j = 0; j < numProducts; ++j)
                    ioInput >> warehouses[i].availableProducts[j];
            }

            ioInput >> numCustomers;
            customers.resize(numCustomers);
            for (int i = 0; i < numCustomers; ++i)
            {
                customers[i].id = i;

                int r0 = 0;
                int c0 = 0;
                int numOrders = 0;

                ioInput >> r0;
                ioInput >> c0;
                customers[i].position = ComputePosition(r0, c0);
                ioInput >> numOrders;
                customers[i].numProducts = numOrders;
                customers[i].orderedProducts.resize(numProducts);
                for (int j = 0; j < numOrders; ++j)
                {
                    int orderedProductId = 0;
                    ioInput >> orderedProductId;
                    ++customers[i].orderedProducts[orderedProductId];
                }
            }

            // Initialize drones
            drones.resize(numDrones);
            for (int i = 0; i < numDrones; ++i)
            {
                auto& drone = drones[i];
                drone.id = i;
                drone.position = warehouses[0].position;
                drone.weight = 0;
                drone.lifeTime = 0;
                drone.loadedProducts.resize(numProducts);
                drone.world = this;
            }

            break;
        }
    }

    void World::Write(std::ostream& ioOutput) const
    {
        ioOutput << instructions.size() << std::endl;

        for (auto instruction : instructions)
            instruction.Print(ioOutput);
    }

    void World::InsertInstruction(const Instruction& iInstruction)
    {
        instructions.push_back(iInstruction);
    }


    int World::ComputeDistance(int iPosition0, int iPosition1, int iWidth)
    {
        int x0 = iPosition0 % iWidth;
        int y0 = iPosition0 / iWidth;

        int x1 = iPosition1 % iWidth;
        int y1 = iPosition1 / iWidth;

        return int(std::ceil(std::sqrt((x0 - x1) * (x0 - x1) + (y0 - y1) * (y0 - y1))));
    }

    int World::ComputeDistance(int iPosition0, int iPosition1) const
    {
        return ComputeDistance(iPosition0, iPosition1, width);
    }

    int World::ComputePosition(int iR, int iC) const
    {
        return iC * width + iR;
    }


} // namespace hc

#endif
