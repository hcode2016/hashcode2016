#ifndef HC_Instruction_H
#define HC_Instruction_H

#include <iostream>

namespace hc
{

    struct Instruction
    {
        enum class Type { eLoad, eDeliver, eWait, eUnload };

        Type type;
        int drone;
        int product;
        int quantity;
        union
        {
            int warehouse;
            int customer;
            int time;
        };


        void Print(std::ostream& ioOutput) const;
    };


    ///////////////////////////////////////////////////////////////////////////
    /// Implementation
    void Instruction::Print(std::ostream& ioOutput) const
    {
        switch (type)
        {
        case Type::eLoad:
            ioOutput << drone << " L " << warehouse << " " << product << " " << quantity << std::endl;
            break;
        case Type::eUnload:
            ioOutput << drone << " U " << warehouse << " " << product << " " << quantity << std::endl;
            break;
        case Type::eDeliver:
            ioOutput << drone << " D " << customer << " " << product << " " << quantity << std::endl;
            break;
        case Type::eWait:
            // ioOutput << drone << " W " <<  << std::endl;
            break;
        default:
            break;
        }
    }

} // namespace hc


#endif
